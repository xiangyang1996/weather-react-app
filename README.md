# Weather React App

An react app periodically check with OpenWeather API to get the latest weather information.


**HOW TO SETUP**
1. Download the code and put into a folder called "weather".
2. Open command prompt and change directory to your "weather" folder.
3. Create build folder by enter: > npm run build 
4. Enter > npm install -g serve
5. Enter > serve -s build
6. Open the generated url in web browser (eg: https://localhost:5000)

**HOW TO USE**
1. This app only allow up to 3 tiles per time.
2. Enter city name in the bottom field
3. Select interval for the refresh time
4. Click "Add City" to add new tiles
5. If city name is invalid, it will show "failed to load city", move your mouse onto the tile and click the remove icon to remove it.
6. If new tile is added successfully, it will show the weather information of the city.
7. When the refresh time at the right bottom is reach, the tile will retrieve new data from OpenWeather and update the information.

Demo Screenshot: https://drive.google.com/file/d/1y009q2JmU9vJXIAii7bSvzpxAW9S_A2K/view?usp=sharing

[END]
