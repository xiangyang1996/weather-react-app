import React, { useEffect, useState, Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/font-awesome/css/font-awesome.min.css'; 
import './App.css';

class Tile extends React.Component {
  constructor(props){
    super(props);
    this.state = { cityname: this.props.cityname, seconds: this.props.interval, info: {} };
    this.timer = 0;

    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
    this.updateWeather.bind(this);

    this.updateWeather();
    this.startTimer();
  }

  startTimer(){
    if (this.timer == 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown(){
    let seconds = this.state.seconds - 1;
    this.setState({
      seconds: seconds
    });
    
    if (seconds == 0) { 
      this.updateWeather();
      this.setState({seconds: this.props.interval});
    }
  }

  updateWeather(){
    fetch("http://api.openweathermap.org/data/2.5/weather?q=" + this.state.cityname + "&appid=31b61e15e4fe9b130a93048872aca792").then(response => response.json()).then(data => this.setState({info: data}));
  }

  render(){
    //console.log(this.state.info);
    if(this.state.info.cod != "200"){
      return(
        <h4>{this.state.info.message}...</h4>
      );
    }
    if(this.state.info.cod == "404"){
      return(
        <h4>Location not found...</h4>
      );
    }
    if(!this.state.info.weather){
      return(
        <h4>Loading...</h4>
      );
    }
    const weather = this.state.info;
    const weatherImgUrl = "http://openweathermap.org/img/wn/" + weather.weather[0].icon + "@2x.png";
    const cloudiness = weather.clouds.all;
    const temperature = (weather.main.temp - 273.15).toFixed(2);
    return(
      <Fragment>
         <h1>{weather.name}</h1>
         <span className="weatherFrame"><img src={weatherImgUrl} /></span>
         <h4>{weather.weather[0].description}</h4>
         <h5><i className="fa fa-cloud"></i> {cloudiness}%</h5>
         <h5><i className="fa fa-thermometer-half"></i> {temperature}°C</h5>
         <span className="refreshTime">Refresh in {this.state.seconds}s</span>
      </Fragment>
    );
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [
        {
          cityname: '',
          interval: 10
        },
        {
          cityname: '',
          interval: 10
        },
        {
          cityname: '',
          interval: 10
        }
      ],
      inputCity: "",
      inputInterval: "10"
    };

    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleIntervalChange = this.handleIntervalChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCityChange(event) {
    this.setState({inputCity: event.target.value});
  }

  handleIntervalChange(event) {
    this.setState({inputInterval: event.target.value});
  }

  handleRemoveItem(index){
    let newCities = this.state.cities;
    newCities[index].cityname = "";
    newCities[index].interval = "10";
    this.setState({cities: newCities});
  }

  handleSubmit(event){
    event.preventDefault();
    let sequence = 0;
    let checkNext = true;
    do{
      if(sequence <= 2){
        if(this.state.cities[sequence].cityname != ""){
          sequence++;
        }
        else{
          checkNext = false;
        }
      }
    } while(checkNext && sequence <= 2);
    if(sequence > 2){
      alert("Maximum display 3 tiles at a time");
    }
    else{
      this.state.cities.splice(sequence, 1);
      let newCities = this.state.cities;
      newCities.push({cityname: this.state.inputCity, interval: this.state.inputInterval});
      this.setState({cities: newCities});
      this.setState({inputCity: ""});
      this.setState({inputInterval: "10"});
    }
  }

  render(){

    //console.log(this.state);
    return (
      <Fragment>
      <div className="board">
        {
          this.state.cities.map((item, index) => {
            if(item.cityname != ""){
              return (
                <Fragment>
                  <div className="tile">
                    <Tile key={item.cityname} cityname={item.cityname} interval={item.interval} tilekey={index} />
                    <span className="removeTile" onClick={this.handleRemoveItem.bind(this,index)}><i className="fa fa-times"></i></span>
                  </div>
                </Fragment>
                )
            }
          })  
        }
      </div>

      <form onSubmit={this.handleSubmit}>
        <label>
          City Name:
          <input type="text" value={this.state.inputCity} onChange={this.handleCityChange} placeholder="Please enter city name" required />
          <select value={this.state.inputInterval} onChange={this.handleIntervalChange}>
            <option value="10">10 seconds</option>
            <option value="30">30 seconds</option>
            <option value="60">60 seconds</option>
            <option value="120">120 seconds</option>
            <option value="300">300 seconds</option>
          </select>
        </label>
        <input type="hidden" value="true" />
        <input type="submit" value="Add City" />
      </form>
      </Fragment>
    );
  } 
}

function CountDownTimer(interval){
  let [count, setCount] = useState(0);

  useEffect(() => {
    let id = setInterval(() => {
      setCount(count + 1);
    }, 1000);
    return () => clearInterval(id);
  });

  return 
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Board />
      </header>
    </div>
  );
}

export default App;
